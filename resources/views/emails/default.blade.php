<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style>
			.btn { display: inline-block; margin: 20px auto; padding: 20px; color: white; background: #48384C; font-weight: bold; border: none; box-shadow: 1px 1px 3px solid rgba(0,0,0,.5); text-decoration: none; }
		</style>
	</head>
	<body style="font-family: arial; font-size: 12pt;">
		<div class="email-page" style="background:rgb(235,235,235);">
			<br>
			<div class="email-content" align="center" style="margin:40px auto; max-width: 520px; width: 100%; font-size: 12pt;">
				<div class="email-header" style="color:white; padding: 20px; background: rgb(131, 142, 112);">
					<a class="navbar-brand header" id="logo" href="{{ env('BASE_URL') }}">
						<img style="border: none; color: white; font-size: 17pt;" src="{{ env('APP_URL') }}/img/logo-white.png" width="200" alt="{{ env('APP_NAME') }}">
					</a>
					<div id="slogan" style="font-size: 10pt;">{{ env('COMPANY_SLOGAN') }}</div>
				</div>
				<div class="email-body" style="padding: 40px; background: white; line-height: 1.5em;">
					@if (isset($user['first_name']))
						<p>{{ $user['first_name'] }},</p>
					@endif
					{!! $email['body'] !!}
					<p id="signature" style="color: gray; font-size: 10pt; line-height: 1.25em;">
						Sincerely,<br>
						The {{ env('APP_NAME') }} team<br>
						Provo, UT, USA<br>
						{{ env('BASE_URL') }}
					</p>
				</div>
				<div class="email-footer" style="padding: 20px; background:rgb(245,245,245); color: gray; font-size: 10pt; line-height: 1.25em;">
					<p>If you have any questions, please contact customer support at <a href="{{ env('BASE_URL') }}/contact">{{ env('BASE_URL') }}/contact</a>.
					<p>You're receiving this message because you or someone else signed up for a subscription at {{ env('APP_NAME') }}. To unsubscribe, <a href="{{ env('APP_URL') }}/unsubscribe?email={{ $user['email'] }}&key={{ $user['key'] }}">click here</a>.</p>
				</div>
			</div>
			<br>
		</div>
	</body>
</html>
