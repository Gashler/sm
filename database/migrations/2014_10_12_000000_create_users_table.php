<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('confirmed')->nullable();
            $table->string('email', 100)->unique();
            $table->text('emails_received')->nullable(); // this should be type "json"; temporarily set to "text" until ready to upgrade MySQL above 5.2 on server
            $table->string('first_name', 100)->nullable();
            $table->string('key', 40)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->float('purchase_amount')->default(0);
            $table->integer('shopify_user_id')->nullable();
            $table->boolean('unsubscribed')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
