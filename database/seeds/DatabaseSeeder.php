<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CampaignsTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(EmailsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
