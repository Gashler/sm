<?php

use App\Models\Campaign;
use App\Models\Email;

class EmailsTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		// prepare data
		$query = Campaign::all();
		foreach ($query as $index => $campaign) {
			$campaigns[$campaign->key] = $campaign;
		}

		// subscription confirmation
		$email = Email::create([
			'key' => 'subscription-confirmation',
			'subject' => "Please confirm your subscription to " . env('APP_NAME'),
			'body' =>
'
<p>
	Thank you for subscribing to {{ env(\'APP_NAME\') }}. To claim your gift certificate, please confirm your email address by clicking below:
</p>
<p>
	<a href="{{ env(\'APP_URL\') }}/confirm?email={{ $user[\'email\'] }}&key={{ $user[\'key\'] }}" class="btn btn-primary">Click here to claim your gift certificate</a>
</p>
'
		]);

		// new users day 1
		$email = Email::create([
			'key' => 'new-user-day-1',
			'subject' => "Your gift certificate has been extended!",
			'body' =>
'
<p>
	We see that you haven\'t gotten around to using your $10 gift certificate yet. We thought you might appreciate a little more time, so the expiration date has been extended another twenty four hours. Just use the code "gift10" at checkout.
</p>
<p>
	If you\'re not ready to buy, feel free to share the code with a friend.
</p>
<p>
	<a href="{{ env(\'BASE_URL\') }}?confirmed=1" class="btn btn-primary">Shop Now</a>
</p>
'
		]);
		$email->campaigns()->save($campaigns['new_users'], ['interval' => 1]);

	}

}
