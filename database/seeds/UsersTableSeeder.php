<?php

use App\Models\User;
use App\Models\Campaign;

class UsersTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$users = [
			[
				'id' => 1,
				'email' => "americanknight@gmail.com"
			]
		];
		User::insert($users);

		// add test user to "new_users" campaign
		$user = User::find(1);
		$campaign = Campaign::where('key', 'new_users')->first();
        $user->campaigns()->save($campaign, ['created_at' => date('Y-m-d h:i:s', strtotime('2 days ago'))]);
	}

}
