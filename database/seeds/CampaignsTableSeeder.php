<?php

use App\Models\Campaign;

class CampaignsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$campaigns = [
			[
				'key' => "new_users",
				'name' => "New Users"
			],
			[
				'key' => "new_customers",
				'name' => "New Customers"
			]
		];
		Campaign::insert($campaigns);
	}
}
