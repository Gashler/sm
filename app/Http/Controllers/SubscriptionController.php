<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Services\MailService;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Subscribe Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MailService $mailService)
    {
        $this->middleware('guest');
        $this->mailService = $mailService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create()
    {
        // validate user
        $data = request()->all();
        $validator = Validator::make($data, ['email' => 'required|string|email|max:255|unique:users']);
        if (count($validator->errors()->all()) > 0) {
            return [
                'errors' => $validator->errors()->all()
            ];
        }

        // create user
        $user = User::create([
            'email' => $data['email'],
            'key' => str_random(40)
        ]);

        // send confirmation email to User
        $this->mailService->send('subscription-confirmation', $user);

        return response([
            'message' => [
                'type' => 'success',
                'body' => "Thank you for subscribing to " .env('APP_NAME') . ". Please check your email to claim your gift certificate. (If you don't see an email, check your SPAM folder and unmark the message as SPAM.)"
            ]
        ], 200);
    }

    /**
    * Confirm an email address
    */
    public function confirm()
    {
        $params = request()->all();
        if ($user = User::where([
            'email' => $params['email'],
            'key' => $params['key']
        ])->first()) {
            $user->update([
                'confirmed' => true
            ]);
            return redirect(env('BASE_URL') . '?confirmed=1');
        }
        return 'Subscriber not found.';
    }

    /**
    * Unsubscribe an email address
    */
    public function unsubscribe()
    {
        $params = request()->all();
        if ($user = User::where([
            'email' => $params['email'],
            'key' => $params['key']
        ])->first()) {
            $user->update([
                'unsubscribed' => true
            ]);
            return "You have been successfully unsubscribed from " . env('APP_URL');
        }
        return 'Subscriber not found.';
    }

    /**
    * Update a user's subscription with shopify data
    */
    public function update()
    {
        $data = request()->all();

        // get user
        if (!$user = User::
                where('shopify_user_id', $data['customer']['id'])
                ->orWhere('email', $data['email'])
                ->orWhere('email', $data['contact_email'])
                ->orWhere('email', $data['customer']['email'])
                ->first()) {
            return response([
                'errors' => ["Can't find user"]
            ], 404);
        }

        // detach user from lists
        $campaign = Campaign::where('key', 'new_users')->first();
        if ($user->campaigns()->find($campaign->id)) {
            $user->campaigns()->detach($campaign->id);
        }

        // add user to lists
        $campaign = Campaign::where('key', 'new_customers')->first();
        if (
            !$campaign->disabled
            && (!$campaign->start_at || $campaign_start_at < date('Y-m-d h:i:s'))
            && (!$campaign->end_at || $campaign_end_at > date('Y-m-d h:i:s'))
        ) {
            if (!$user->campaigns()->find($campaign->id)) {
                $user->campaigns()->save($campaign);
            }
        }

        // prepare data
        if (isset($data['contact_email'])) {
            $email = $data['contact_email'];
        } else {
            $email = $data['email'];
        }

        // update user
        $user->update([
            'customer' => true,
            'email' => $email,
            'first_name' => $data['customer']['first_name'],
            'last_name' => $data['customer']['last_name'],
            'purchase_amount' => $user->purchase_amount += $data['subtotal_price'],
            'shopify_user_id' => $data['user_id'],
            'city' => $data['shipping_address']['city'],
            'province' => $data['shipping_address']['province'],
            'country' => $data['shipping_address']['country']
        ]);

        return response([
            'message' => [
                'type' => 'success',
                'body' => "Updated user."
            ]
        ], 200);
    }
}
