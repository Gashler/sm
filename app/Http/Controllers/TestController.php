<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\User;

use App\Http\Controllers\Controller;
use App\Services\CurlService;
use App\Services\MailService;

class TestController extends Controller
{

    public function __construct(
        CurlService $curlService,
        MailService $mailService
    ) {
        $this->curlService = $curlService;
        $this->mailService = $mailService;
    }

    /**
     * Run tests
     */
    public function index()
    {
        // $email = [
        //     'body' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae sapien consequat, tempor est scelerisque, rhoncus neque. Nunc maximus aliquam nisl at tristique. Fusce lobortis, eros non auctor sodales, purus felis accumsan turpis, eu luctus justo ante at mauris. Mauris vel ante elit. Aliquam erat volutpat. Nulla faucibus, libero et auctor dignissim, lorem nunc aliquam nulla, eget posuere dui leo id dui. Quisque vulputate leo diam, a elementum tortor ornare id. Nam varius vehicula egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus et lacus viverra, tristique lectus eu, commodo turpis. Pellentesque in justo ac purus lobortis aliquam. Quisque ut augue hendrerit, congue eros sed, pellentesque turpis. Etiam placerat, lacus nec iaculis mattis, libero leo molestie arcu, eu congue felis arcu id sem. Sed tempor vehicula porttitor. Donec rutrum, lorem eget cursus iaculis, dolor urna iaculis quam, tincidunt tristique massa ipsum vel eros. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
        // ];
        $user = [
            'first_name' => 'Steve',
            'email' => 'americanknight@gmail.com',
            'key' => "asfjljweofnvsnhoiffweoifjeflsdvnew"
        ];
        return $this->mailService->send('subscription-confirmation', $user);



        /**********************
        * Test Email Template
        ***********************/
        // $email = [
        //     'body' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae sapien consequat, tempor est scelerisque, rhoncus neque. Nunc maximus aliquam nisl at tristique. Fusce lobortis, eros non auctor sodales, purus felis accumsan turpis, eu luctus justo ante at mauris. Mauris vel ante elit. Aliquam erat volutpat. Nulla faucibus, libero et auctor dignissim, lorem nunc aliquam nulla, eget posuere dui leo id dui. Quisque vulputate leo diam, a elementum tortor ornare id. Nam varius vehicula egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus et lacus viverra, tristique lectus eu, commodo turpis. Pellentesque in justo ac purus lobortis aliquam. Quisque ut augue hendrerit, congue eros sed, pellentesque turpis. Etiam placerat, lacus nec iaculis mattis, libero leo molestie arcu, eu congue felis arcu id sem. Sed tempor vehicula porttitor. Donec rutrum, lorem eget cursus iaculis, dolor urna iaculis quam, tincidunt tristique massa ipsum vel eros. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
        // ];
        // $user = [
        //     'email' => 'americanknight@gmail.com',
        //     'key' => "asfjljweofnvsnhoiffweoifjeflsdvnew"
        // ];
        // return view('emails.default', compact('email', 'user'));
    }
}
