<?php

namespace App\Models;

class Campaign extends \Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'disabled',
        'name',
        'start_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
    * Relationships
    */
    public function emails()
    {
        return $this->belongsToMany(Email::class)->withPivot('interval');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('created_at');
    }
}
