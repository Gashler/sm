<?php

namespace App\Models;

class EmailTemplate extends \Eloquent
{
    protected $table = 'email_templates';
    protected $fillable = [
        'key',
    ];
}
