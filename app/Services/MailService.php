<?php

namespace App\Services;

use Log;
use Mail;
use App\Models\Email;

use App\Services\CurlService;

class MailService extends CurlService
{
    // request an endpoint via CurlService
    public function request($endpoint, $method = 'GET', $body = null)
    {
        $url = config('services.sparkpost.url') . "/" . $endpoint;
        $headers = [
            "x-auth-token: api-key " . config("services.sparkpost.key"),
            "content-type: application/json"
        ];
        $response = $this->curl($method, $url, $headers, $body);

        // if there's a curl error
        if ($response['error']) {
            return $response;
        }

        // handle service errors
        $response['response'] = json_decode($response['response']);
        if (isset($response['response']->httpStatus) && $response['response']->httpStatus !== 200) {
            return [
                'error' => true,
                'response' => $response['response']
            ];
        }
        return [
            'error' => false,
            'response' => $response['response']
        ];
    }

    // add a contact
    public function send($emailKey, $user = null)
    {
        if (!$user) {
            $user = auth()->user();
        }
        $email = Email::where('key', $emailKey)->orWhere('id', $emailKey)->first();
        $array = explode('{{', $email['body']);
        foreach ($array as $index => $str) {
            if (strpos($str, '}}') !== false) {
                $array2 = explode('}}', $str);
                foreach ($array2 as $index2 => $str2) {
                    if ($index2 % 2 == 0) {
                        eval('$array2[$index2] = ' . $str2 . ';');
                    }
                }
                $array[$index] = implode($array2);
            }
        }
        $email['body'] = implode($array);
        Mail::send('emails.' . $email->template->key, [
            'user' => $user,
            'email' => $email
        ], function ($m) use ($user, $email) {
            $m->to($user['email'], $user['first_name'])->subject($email['subject']);
        });
        return true;
    }
}
