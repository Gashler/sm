<?php

namespace App\Console\Commands;

use Log;

use App\Models\Campaign;
use App\Models\User;
use App\Services\MailService;
use Illuminate\Console\Command;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MailService $mailService)
    {
        parent::__construct();
        $this->mailService = $mailService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaigns = Campaign::with([
            'emails',
            'users'
        ])->where('disabled', 0)
        ->where(function($query) {
            $query->whereNull('start_at')
                ->orWhere('start_at', '<=', date('Y-m-d h:i:s'));
        })->where(function($query) {
            $query->whereNull('end_at')
                ->orWhere('end_at', '>=', date('Y-m-d h:i:s'));
        })->get();
        foreach ($campaigns as $campaign) {
            foreach ($campaign->users as $user) {
                $sentToUserThisCycle = false; // limit to 1 email per user per cycle
                foreach ($campaign->emails as $email) {
                    if (
                        $user->confirmed
                        && !$user->unsubscribed
                        && !$sentToUserThisCycle
                        && (!$user->emails_received || !in_array($email->id, $user->emails_received))
                    ) {
                        $daysSinceSubscribing = round((time() - strtotime($user->pivot->created_at)) * 86400);
                        if ($email->pivot->interval == $daysSinceSubscribing) {
                            if ($sentToUserThisCycle = $this->mailService->send($email->key, $user)) {
                                $emails_received = $user->emails_received;
                                $emails_received[] = $email->id;
                                $user->update([
                                    'emails_received' => $emails_received
                                ]);
                            }
                        }
                    }
                }
            }
        }
        Log::info('Campaign emails sent');
    }
}
