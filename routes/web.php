<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('subscribe', 'SubscriptionController@create');
Route::get('confirm', 'SubscriptionController@confirm');
Route::get('unsubscribe', 'SubscriptionController@unsubscribe');
Route::post('orders/update', 'SubscriptionController@update');
Route::resource('test', 'TestController');
